package Lab4;

/**
 * Class Faculty inherits employee
 * adds string[] courses
 */
public class Faculty extends Employee {
    private String[]courses;
    public Faculty(String aName, int aSalary, String []newCourses) {
        super(aName, aSalary);
        courses = newCourses;
    }
    /**
     * Overrides employee name
     * @return string with modified name
     */
    @Override
    public String getName(){
        return "Professor " + super.getName();
    }
    /**
     * Set courses to input course list
     * @param courses the array to hold courses
     * @return the filled array
     */

    public void setCourses(String[]courses){
        this.courses = new String[courses.length];
        for(int i=0; i<courses.length; i++)
            this.courses[i] = courses[i];
    }
    /**
     * Get courses returns list of courses
     * @return course array
     */
    public String[] getCourses(){
       String [] newCourses = new String[courses.length];
       for(int i=0; i<courses.length; i++)
           newCourses[i]=new String(courses[i]);
       return newCourses;
    }

    /**
     * To string returns course array as a string
     * @return name and course list
     */
    public String toString(){
        String output = "";
        for(int i=0; i<courses.length; i++){
            output +=courses[i]+" ";
        }
        return getName()+"\t"+output;
    }

}
