package Lab4;

/**
 * Class employee to implement employee rules
 * adds fields salary and name
 */
public class Employee implements EmployeeRules {
    private int salary;
    private String name;
    /**
     * default constructor
     */
    public Employee(){
    }
    /**
     * Constructor for employees
     * @param aName holds name input
     * @param aSalary holds salary input
     */
    public Employee(String aName, int aSalary) {
        salary = aSalary;
        name = aName;
    }
    /**
     * Return name of employee
     * @return name
     */
    public String getName(){
        return name;
    }
    /**
     * Return salary of employee
     * @return salary
     */
    public int getSalary(){
        return salary;
    }

    /**
     * To string prints name and salary of employee
     * @return information about employee
     */
    public String toString(){
        String information = name + "\t" + salary;
        return information;
    }
}
