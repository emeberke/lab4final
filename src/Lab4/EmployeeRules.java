package Lab4;

/**
 * interface holds rules for getname and getsalary methods
 */
public interface EmployeeRules {
    String getName();
    int getSalary();

}
